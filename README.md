# Teatro Tecnológico

El Teatro Tecnológico es un dispositivo electrónico
que permite el control automatizado de luces para
que pueden utilizarse para puestas en escena de teatro
o para activar de forma automatica iluminación

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Teatro Tecnológico" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Módulo de Relé                  |        4 |
| Botón táctil                    |        2 |
| Sensor de sonidos               |        1 |
| Breadboard                      |        1 |
| Cable Jumper M/M                |       12 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **SO** se conectan
al sensor de sonidos.

| Componente                         |        |         |        |        
|------------------------------------|--------|---------|--------|
| Jumper 1                           | AR-GND | BB-V-   |        |
| Botón táctil 1                     | BB-H6  | BB-H8   |        |
| Botón táctil 2                     | BB-H10 | BB-H12  |        |
| Jumper 2                           | BB-A8  | AR-9    |        |
| Jumper 3                           | BB-A12 | AR-8    |        |
| Módulo de Relé 1                   | BB-V-  | AR-5    | BB-V+  |
| Módulo de Relé 2                   | BB-V-  | AR-4    | BB-V+  |
| Módulo de Relé 3                   | BB-V-  | AR-3    | BB-V+  |
| Módulo de Relé 4                   | BB-V-  | AR-2    | BB-V+  |
| Jumper 4 (Conectar de lado a lado) | BB-V+  | BB-V+   |        |
| Jumper 5 (Conectar de lado a lado) | BB-V-  | BB-V-   |        |
| Jumper 6                           | SO-VCC | BB-V+   |        |
| Jumper 7                           | SO-GND | BB-V-   |        |
| Jumper 8                           | SO-ENV | AR-A0   |        |
| Jumper 9                           | SO-GAT | BB-A1   |        |
| Jumper 10                          | AR-5V  | BB-V+   |        |
| Jumper 11                          | BB-J6  | BB-V-   |        |
| Jumper 12                          | BB-J11 | BB-V-   |        |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_e2***.
