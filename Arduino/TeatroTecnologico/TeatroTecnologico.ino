const int rele1 = 2;
const int rele2 = 3;
const int rele3 = 4;
const int rele4 = 5;
const int btn1 = 8;
const int btn2 = 9;
const int entrada_analoga = A1; // En A0 esta la señal de audio

const int muestras = 200; // Numero de muestras de audio
const int retardo_muestras = 1; // Retardo entre muestras

const int corte_banco1 = 85;
const int corte_banco2 = 170;

int estado_banco1 = 0;
int estado_banco2 = 0;

uint8_t muestra_actual = 0;
uint8_t muestras_audio[muestras];

void setup() {
  pinMode(rele1, OUTPUT);
  pinMode(rele2, OUTPUT);
  pinMode(rele3, OUTPUT);
  pinMode(rele4, OUTPUT);
  pinMode(btn1, INPUT_PULLUP);
  pinMode(btn2, INPUT_PULLUP);
}

void loop() {
  actualizar_muestra();
  
  if(digitalRead(btn1)==LOW) { // Captura botón 1 presionado
    if(estado_banco1==2) {
      estado_banco1 = 0;
    } else {
      estado_banco1++;
    }
    delay(250);
  }
  if(digitalRead(btn2)==LOW) { // Captura botón 2 presionado
    if(estado_banco2==2) {
      estado_banco2 = 0;
    } else {
      estado_banco2++;
    }
    delay(250);
  }
  
  if(estado_banco1 == 0) { // Luces apagadas
    digitalWrite(rele1, LOW);
    digitalWrite(rele2, LOW);
  }
  if(estado_banco1 == 1) { // Luces encendidas
    digitalWrite(rele1, HIGH);
    digitalWrite(rele2, HIGH);
  }
  if(estado_banco1 == 2) { // Luces activadadas por sonido
    int promedio = promedio_muestras();
    if(promedio>corte_banco1) {
      digitalWrite(rele1, HIGH);
      digitalWrite(rele2, HIGH);
    } else {
      digitalWrite(rele1, LOW);
      digitalWrite(rele2, LOW); 
    }
  }
  
  if(estado_banco2 == 0) { // Luces apagadas
    digitalWrite(rele3, LOW);
    digitalWrite(rele4, LOW);
  }
  if(estado_banco2 == 1) { // Luces encendidas
    digitalWrite(rele3, HIGH);
    digitalWrite(rele4, HIGH);
  }
  if(estado_banco2 == 2) { // Luces activadadas por sonido
    int promedio = promedio_muestras();
    if(promedio>corte_banco2) {
      digitalWrite(rele3, HIGH);
      digitalWrite(rele4, HIGH);
    } else {
      digitalWrite(rele3, LOW);
      digitalWrite(rele4, LOW); 
    }
  }

  delay(retardo_muestras);
}

void actualizar_muestra() {
 // Actualiza una muestra de audio:
 if(muestra_actual>=200) {
  muestra_actual = 0;
 }
 muestras_audio[muestra_actual++] = map(analogRead(A1),0,1024,0,255);
}

uint8_t promedio_muestras() {
  uint16_t suma = 0;
  for(int i=0;i<muestras;i++) {
    suma += muestras_audio[i];
  }
  return suma/muestras;
}
